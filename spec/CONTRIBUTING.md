# Contributing to GitLab Kramdown

This gem implements GitLab Flavored Markdown extension on the Kramdown parser.

As not all features are possible to implement, without API or Database access,
we are aiming to get any syntax feature that doesn't require that first
and think carefully how, or if we will, implement the others.

Even if we decide to do so, it will be optional to enable them, and we should
have a fallback for when offline or when we can't access the data-source.

## Regular Expressions

The most important part of implementing new syntax features is to figure out
a correct regular expression, that is also fast enough.

Here is a list of useful resources on Ruby specific (Onigurama) RegExp engine:

* [Programming Ruby 1.9 - Chapter 7 - Regular Expressions](http://media.pragprog.com/titles/ruby3/ruby3_extract_regular_expressions.pdf)
* [Oniguruma - Regular Expressions documentation](https://github.com/kkos/oniguruma/blob/master/doc/RE)
* [Rubular](https://rubular.com/) - You can test Ruby expressions online
* [Regex101](https://regex101.com) - Select PCRE (PHP) as your Flavor - 
  It's very similar to Oniguruma, and provides nice explanations.
